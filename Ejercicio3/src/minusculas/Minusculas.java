package minusculas;

import java.util.Scanner;

public class Minusculas {

	public static void main(String[] args) {
		
		Scanner input=new Scanner(System.in);
		
		System.out.println("Introduce una frase para pasarla a minúsculas");
		String frase=input.nextLine().toLowerCase();
		
		System.out.println(frase);
		
		
		input.close();

	}

}
